// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//Test comment on 28Aug - please delete noon
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.5.0

namespace CoreBot2._1
{
    public class BookingDetails
    {
        public string Destination { get; set; }

        public string Origin { get; set; }

        public string TravelDate { get; set; }
    }
}
